package com.tuncaysenturk.jira.plugins;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.web.util.OutlookDate;

import java.sql.Timestamp;
/**
 * @author tuncay.senturk
 */
public class ReopenedIssue {
	private User resolvedUser;
	private User reOpenedUser;
	private Timestamp resolvedTime;
	private Timestamp reOpenedTime;
	private OutlookDate outlookDate;

	public ReopenedIssue(OutlookDate outlookDate, User resolvedUser, User reOpenedUser, Timestamp resolvedTime, Timestamp reOpenedTime) {
		super();
		this.resolvedUser = resolvedUser;
		this.reOpenedUser = reOpenedUser;
		this.resolvedTime = resolvedTime;
		this.reOpenedTime = reOpenedTime;
		this.outlookDate = outlookDate;
	}
	public Timestamp getReOpenedTime() {
		return reOpenedTime;
	}
	public void setReOpenedTime(Timestamp reOpenedTime) {
		this.reOpenedTime = reOpenedTime;
	}
	public User getReOpenedUser() {
		return reOpenedUser;
	}
	public void setReOpenedUser(User reOpenedUserName) {
		this.reOpenedUser = reOpenedUserName;
	}
	public Timestamp getResolvedTime() {
		return resolvedTime;
	}
	public void setResolvedTime(Timestamp resolvedTime) {
		this.resolvedTime = resolvedTime;
	}
	public User getResolvedUser() {
		return resolvedUser;
	}
	public void setResolvedUser(User resolvedUserName) {
		this.resolvedUser = resolvedUserName;
	}
	
	public String toString() {
		return new StringBuffer(
				(resolvedUser != null ? resolvedUser.getDisplayName() : ""))
				.append("|")
				.append(resolvedTime != null ? resolvedTime.toString() : "")
				.append("|")
				.append(reOpenedUser != null ? reOpenedUser.getDisplayName() : "")
				.append("|")
				.append(reOpenedTime != null ? reOpenedTime.toString() : "")
				.toString();
	}
	public OutlookDate getOutlookDate() {
		return outlookDate;
	}
	public void setOutlookDate(OutlookDate outlookDate) {
		this.outlookDate = outlookDate;
	}
	
}
