/**
 * @author tsenturk
 * Abstract custom field 
 * Developer : Tuncay Senturk
 */

package com.tuncaysenturk.jira.plugins;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.customfields.SortableCustomField;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.security.JiraAuthenticationContext;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericEntity;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

@SuppressWarnings({ "unchecked", "rawtypes" })
public abstract class AbstractJepCFType<T> extends CalculatedCFType implements SortableCustomField {
	protected static final Logger logger = Logger.getLogger(AbstractJepCFType.class);
    protected final ChangeHistoryManager changeHistoryManager;
    protected final JiraAuthenticationContext authenticationContext;
    protected int CUSTOM_FIELD_STATUS_ID;
    
    private final DateTimeFormatter datePickerFormatter;

    public AbstractJepCFType(UserConverter userConverter, ChangeHistoryManager actionManager, JiraAuthenticationContext authenticationContext, DateTimeFormatterFactory dateTimeFormatterFactory) {
        this.changeHistoryManager = actionManager;
        this.authenticationContext = authenticationContext;
        this.datePickerFormatter = dateTimeFormatterFactory.formatter().forLoggedInUser().withStyle(DateTimeStyle.DATE_TIME_PICKER);
    }

    public abstract String getStringFromSingularObject(Object value);

    public abstract Object getSingularObjectFromString(String string) throws FieldValidationException;

    /** 
     * implement this method to get issue's last closed date.
     * @see com.atlassian.jira.issue.customfields.CustomFieldType#getValueFromIssue(com.atlassian.jira.issue.fields.CustomField, com.atlassian.jira.issue.Issue)
     */
    public Object getValueFromIssue(CustomField field, Issue issue) {
        User currentUser = authenticationContext.getLoggedInUser();
        Timestamp date = null;
        try {
        	//Get all change histories of the issue
        	List<ChangeHistory> changes = changeHistoryManager.getChangeHistoriesForUser(issue, currentUser);
        	for (int i = 0; i < changes.size(); i++) {
        		//iterate the issues last to first so that we reach the actions by date descending
        		ChangeHistory ch = changes.get(changes.size() - 1 - i);
        		List<GenericEntity> changeItems = ch.getChangeItems();
        		//Every change history has change items. We will look at the values in change items.
        		for (int j = 0; j < changeItems.size(); j++) {
        			GenericEntity changeItem = changeItems.get(changeItems.size() - 1 - j);
        			//Get change item's field property (if it has)
        			String ciField = (changeItem.containsKey("field") ? changeItem.getString("field") : "");
        			if (IssueFieldConstants.STATUS.equals(ciField)) {
        				//if status has changed (it can be "issue closed" action)
        				int ciNewValue = Integer.parseInt(changeItem.containsKey("newvalue") ? changeItem.getString("newvalue") : "0");
        				if (CUSTOM_FIELD_STATUS_ID == ciNewValue) {
	        				return ch;
        				}
        			}
        		}
        	}
        } catch (Exception e) {
        	logger.error("exception while getting change history", e);
        }
        return date;
    }
    
    @Override
	public Map<String, Object> getVelocityParameters(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem) {
		Map<String, Object> context = super.getVelocityParameters(issue, field, fieldLayoutItem);
		context.put("datePickerFormatter", datePickerFormatter);
		context.put("titleFormatter", datePickerFormatter.withStyle(DateTimeStyle.COMPLETE));
		context.put("iso8601Formatter", datePickerFormatter.withStyle(DateTimeStyle.ISO_8601_DATE_TIME));
		return context;
    }

}
