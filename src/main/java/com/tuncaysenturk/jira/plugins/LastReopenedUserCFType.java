package com.tuncaysenturk.jira.plugins;
/**
 * @author tuncay.senturk
 */
import java.util.List;

import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.UserField;
import com.atlassian.jira.security.JiraAuthenticationContext;

@SuppressWarnings("unchecked")
public class LastReopenedUserCFType extends ReopenedIssueCFType implements UserField {
    
    public LastReopenedUserCFType(UserConverter userConverter, ChangeHistoryManager changeHistoryManager, JiraAuthenticationContext authenticationContext, DateTimeFormatterFactory dateTimeFormatterFactory) {
    	super(userConverter, changeHistoryManager, authenticationContext, dateTimeFormatterFactory);
    }
    public Object getValueFromIssue(CustomField customField, Issue issue) {
        List<ReopenedIssue> list = (List<ReopenedIssue>) super.getValueFromIssue(customField, issue);
        if (null == list || list.size() == 0)
        	return list;
        return ((ReopenedIssue)list.get(list.size() - 1)).getReOpenedUser();
    }
}
