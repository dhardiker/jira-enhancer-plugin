/**
 * @author tsenturk
 * Abstract user custom field 
 * Developer : Tuncay Senturk
 */

package com.tuncaysenturk.jira.plugins;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.comparator.UserBestNameComparator;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.UserField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.security.JiraAuthenticationContext;

@SuppressWarnings("unchecked")
public class AbstractJepUserCFType extends AbstractJepCFType<User> implements UserField {
	public AbstractJepUserCFType(UserConverter userConverter,
			ChangeHistoryManager actionManager,
			JiraAuthenticationContext authenticationContext, DateTimeFormatterFactory dateTimeFormatterFactory) {
		super(userConverter, actionManager, authenticationContext, dateTimeFormatterFactory);
	}

    public String getStringFromSingularObject(Object value) {
        assertObjectImplementsType(User.class, value);
        return ((User)value).getName();
    }

    public Object getSingularObjectFromString(String string) throws FieldValidationException {
    	return authenticationContext.getLoggedInUser();
    }

    public Object getValueFromIssue(CustomField field, Issue issue) {
    	ChangeHistory ch = ((ChangeHistory)super.getValueFromIssue(field, issue));
    	if (ch != null)
    		return ch.getAuthorUser();
    	else 
    		return null;
    }
    
    public int compare(Object customFieldObjectValue1, Object customFieldObjectValue2, FieldConfig customFieldConfig) {
        //FIXME: Allt hese UserCFType objects need to be generified
        return new UserBestNameComparator().compare((User)customFieldObjectValue1, (User)customFieldObjectValue2);
    }
}
