/**
 * @author tsenturk
 * MKK JIRA Plugin @version 1.0
 * Represents Last User that resolved the issue. 
 * Developer : Tuncay Senturk
 */

package com.tuncaysenturk.jira.plugins;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.comparator.UserBestNameComparator;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.security.JiraAuthenticationContext;
import org.ofbiz.core.entity.GenericEntity;

import java.util.List;

@SuppressWarnings({ "rawtypes" })
public class LastResolvedUserCFType extends AbstractJepUserCFType {

	public LastResolvedUserCFType(UserConverter userConverter,
			ChangeHistoryManager actionManager,
			JiraAuthenticationContext authenticationContext, DateTimeFormatterFactory dateTimeFormatterFactory) {
		super(userConverter, actionManager, authenticationContext, dateTimeFormatterFactory);
	}

    /** 
     * implement this method to get issue's last resolved user.
     * @see com.atlassian.jira.issue.customfields.CustomFieldType#getValueFromIssue(com.atlassian.jira.issue.fields.CustomField, com.atlassian.jira.issue.Issue)
     */
    public Object getValueFromIssue(CustomField field, Issue issue) {
        User currentUser = authenticationContext.getLoggedInUser();
        User lastResolvedUser = null;
        try {
        	List changes = changeHistoryManager.getChangeHistoriesForUser(issue, currentUser);
        	for (int i = 0; i < changes.size(); i++) {
        		ChangeHistory ch = (ChangeHistory) changes.get(changes.size() - 1 - i);
        		List changeItems = ch.getChangeItems();
        		for (int j = 0; j < changeItems.size(); j++) {
        			GenericEntity changeItem = (GenericEntity) changeItems.get(changeItems.size() - 1 - j);
        			String ciField = (changeItem.containsKey("field") ? changeItem.getString("field") : "");
        			if (IssueFieldConstants.RESOLUTION.equals(ciField)) {
        				// is resolution changed?
        				Object newValue = changeItem.get("newvalue");
        				Object oldValue = changeItem.get("oldvalue");
        				if (null != newValue && null == oldValue) {
        					return ch.getAuthorUser();
        				}
        			}
        		}
        	}
        } catch (Exception e) {
        	logger.error("exception while getting change history", e);
        }
        return lastResolvedUser;
    }

    public int compare(Object customFieldObjectValue1, Object customFieldObjectValue2, FieldConfig customFieldConfig) {
        return new UserBestNameComparator().compare((User)customFieldObjectValue1, (User)customFieldObjectValue2);
    }
}
