/**
 * @author tsenturk
 * JIRA Enhancer Plugin 
 * Custom field represents last date that a user 
 * 	changed status of the issue to "In Progress" 
 * Developer : Tuncay Senturk
 */

package com.tuncaysenturk.jira.plugins;

import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.security.JiraAuthenticationContext;

@SuppressWarnings("unchecked")
public class LastInProgressDateCFType extends AbstractJepDateCFType {

	public LastInProgressDateCFType(UserConverter userConverter,
			ChangeHistoryManager actionManager,
			JiraAuthenticationContext authenticationContext, DateTimeFormatterFactory dateTimeFormatterFactory) {
		super(userConverter, actionManager, authenticationContext, dateTimeFormatterFactory);
		this.CUSTOM_FIELD_STATUS_ID = IssueFieldConstants.INPROGRESS_STATUS_ID;
	}
}
