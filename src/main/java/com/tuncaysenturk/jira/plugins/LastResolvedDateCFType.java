/**
 * @author tsenturk
 * MKK JIRA Plugin @version 1.0
 * Represents Last Resolved Date of the issue. 
 * Developer : Tuncay Senturk
 */

package com.tuncaysenturk.jira.plugins;

import java.sql.Timestamp;
import java.util.List;

import org.ofbiz.core.entity.GenericEntity;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.security.JiraAuthenticationContext;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class LastResolvedDateCFType extends AbstractJepDateCFType {

    public LastResolvedDateCFType(UserConverter userConverter,
			ChangeHistoryManager actionManager,
			JiraAuthenticationContext authenticationContext, DateTimeFormatterFactory dateTimeFormatterFactory) {
		super(userConverter, actionManager, authenticationContext, dateTimeFormatterFactory);
	}

	/** 
     * implement this method to get issue's last resolved date.
     * @see com.atlassian.jira.issue.customfields.CustomFieldType#getValueFromIssue(com.atlassian.jira.issue.fields.CustomField, com.atlassian.jira.issue.Issue)
     */
    public Object getValueFromIssue(CustomField field, Issue issue) {
        User currentUser = authenticationContext.getLoggedInUser();
        Timestamp date = null;
        try {
        	//Get all change histories of the issue
        	List changes = changeHistoryManager.getChangeHistoriesForUser(issue, currentUser);
        	for (int i = 0; i < changes.size(); i++) {
        		//iterate the issues last to first so that we reach the actions by date descending
        		ChangeHistory ch = (ChangeHistory) changes.get(changes.size() - 1 - i);
        		List changeItems = ch.getChangeItems();
        		//Every change history has change items. We will look at the values in change items.
        		for (int j = 0; j < changeItems.size(); j++) {
        			GenericEntity changeItem = (GenericEntity) changeItems.get(changeItems.size() - 1 - j);
        			//Get change item's field property (if it has)
        			String ciField = (changeItem.containsKey("field") ? changeItem.getString("field") : "");
        			if (IssueFieldConstants.RESOLUTION.equals(ciField)) {
        				// is resolution changed?
        				Object newValue = changeItem.get("newvalue");
        				Object oldValue = changeItem.get("oldvalue");
        				if (null != newValue && null == oldValue) {
        					return ch.getTimePerformed();
        				}
        			}
        		}
        	}
        } catch (Exception e) {
        	logger.error("exception while getting change history", e);
        }
        return date;
    }
}
