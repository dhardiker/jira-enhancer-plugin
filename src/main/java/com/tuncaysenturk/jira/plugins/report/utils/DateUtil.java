package com.tuncaysenturk.jira.plugins.report.utils;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class DateUtil
{

    public static final String ISO_DATE_FORMAT = "yyyyMMdd";
    public static final String ISO_EXPANDED_DATE_FORMAT = "yyyy-MM-dd";
    public static final String ISO_TIME_FORMAT = "HHmmssSSSzzz";
    public static final String ISO_EXPANDED_TIME_FORMAT = "HH:mm:ss,SSSzzz";
    public static final String ISO_DATE_TIME_FORMAT = "yyyyMMdd'T'HHmmssSSSzzz";
    public static final String ISO_EXPANDED_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss,SSSzzz";
    public static final DateFormatSymbols dateFormatSymbles;
    private static final String foo[][];

    public DateUtil() {}

    public static final boolean isLeapYear(String isoString, boolean expanded)
        throws ParseException 
    {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(isoToDate(isoString, expanded));
        return cal.isLeapYear(cal.get(1));
    }

    public static final boolean isLeapYear(String isoString)
        throws ParseException
    {
        return isLeapYear(isoString, false);
    }

    public static final TimeZone getTimeZoneFromDateTime(String date, boolean expanded)
        throws ParseException
    {
        SimpleDateFormat formatter;
        if(expanded)
            formatter = new SimpleDateFormat("yyyy-MM-dd", dateFormatSymbles);
        else
            formatter = new SimpleDateFormat("yyyyMMdd", dateFormatSymbles);
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        formatter.parse(date);
        return formatter.getTimeZone();
    }

    public static final TimeZone getTimeZoneFromDateTime(String date)
        throws ParseException
    {
        return getTimeZoneFromDateTime(date, false);
    }

    public static final String add(String isoString, int field, int amount, boolean expanded)
        throws ParseException
    {
        Calendar cal = GregorianCalendar.getInstance(TimeZone.getTimeZone("GMT"));
        cal.setTime(isoToDate(isoString, expanded));
        cal.add(field, amount);
        return dateToISO(cal.getTime(), expanded);
    }

    public static final String add(String isoString, int field, int amount)
        throws ParseException
    {
        return add(isoString, field, amount, false);
    }

    public static final String dateToISO(Date date, boolean expanded)
    {
        SimpleDateFormat formatter;
        if(expanded)
            formatter = new SimpleDateFormat("yyyy-MM-dd", dateFormatSymbles);
        else
            formatter = new SimpleDateFormat("yyyyMMdd", dateFormatSymbles);
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        return formatter.format(date);
    }

    public static final String dateToISO(Date date)
    {
        return dateToISO(date, false);
    }

    public static final long dateToJulianDay(Date date)
    {
        return millisToJulianDay(date.getTime());
    }

    /**
     * @deprecated Method daysBetween is deprecated
     */

    public static final int daysBetween(Date early, Date late)
    {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c1.setTime(early);
        c2.setTime(late);
        return daysBetween(c1, c2);
    }

    /**
     * @deprecated Method daysBetween is deprecated
     */

    public static final int daysBetween(Calendar early, Calendar late)
    {
        return (int)(toJulian(late) - toJulian(early));
    }

    public static final long daysBetween(String isoEarly, String isoLate, boolean expanded)
        throws ParseException
    {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c1.setTimeZone(getTimeZoneFromDateTime(isoEarly, expanded));
        c1.setTime(isoToDate(isoEarly, expanded));
        c2.setTimeZone(getTimeZoneFromDateTime(isoLate, expanded));
        c2.setTime(isoToDate(isoLate, expanded));
        return millisToJulianDay(c2.getTime().getTime()) - millisToJulianDay(c1.getTime().getTime());
    }

    public static final Date isoToDate(String dateString, boolean expanded)
        throws ParseException
    {
        SimpleDateFormat formatter;
        if(expanded)
            formatter = new SimpleDateFormat("yyyy-MM-dd", dateFormatSymbles);
        else
            formatter = new SimpleDateFormat("yyyyMMdd", dateFormatSymbles);
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        return new Date(formatter.parse(dateString).getTime());
    }

    public static final Date isoToDate(String dateString)
        throws ParseException
    {
        return isoToDate(dateString, false);
    }

    public static final java.sql.Date isoToSQLDate(String dateString, boolean expanded)
        throws ParseException
    {
        SimpleDateFormat formatter;
        if(expanded)
            formatter = new SimpleDateFormat("yyyy-MM-dd", dateFormatSymbles);
        else
            formatter = new SimpleDateFormat("yyyyMMdd", dateFormatSymbles);
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        return new java.sql.Date(formatter.parse(dateString).getTime());
    }

    public static final java.sql.Date isoToSQLDate(String dateString)
        throws ParseException
    {
        return isoToSQLDate(dateString, false);
    }

    public static final Time isoToTime(String dateString, boolean expanded)
        throws ParseException
    {
        SimpleDateFormat formatter;
        if(expanded)
            formatter = new SimpleDateFormat("HH:mm:ss,SSSzzz", dateFormatSymbles);
        else
            formatter = new SimpleDateFormat("HHmmssSSSzzz", dateFormatSymbles);
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        return new Time(formatter.parse(dateString).getTime());
    }

    public static final Time isoToTime(String dateString)
        throws ParseException
    {
        return isoToTime(dateString, false);
    }

    public static final Timestamp isoToTimestamp(String dateString, boolean expanded)
        throws ParseException
    {
        SimpleDateFormat formatter;
        if(expanded)
            formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss,SSSzzz", dateFormatSymbles);
        else
            formatter = new SimpleDateFormat("yyyyMMdd'T'HHmmssSSSzzz", dateFormatSymbles);
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        return new Timestamp(formatter.parse(dateString).getTime());
    }

    public static final Timestamp isoToTimestamp(String dateString)
        throws ParseException
    {
        return isoToTimestamp(dateString, false);
    }

    public static final java.sql.Date julianDayCountToDate(long julian)
    {
        return new java.sql.Date(julianDayToMillis(julian));
    }

    public static final Date julianDayToDate(long julian)
    {
        return new Date(julianDayToMillis(julian));
    }

    public static final long julianDayToMillis(long julian)
    {
        return ((julian - 0x253d8cL) + 0x1a4452L) * 0x5265c00L;
    }

    public static final long millisToJulianDay(long millis)
    {
        return 0xaf93aL + millis / 0x5265c00L;
    }

    public static final String roll(String isoString, int field, boolean up, boolean expanded)
        throws ParseException
    {
        Calendar cal = GregorianCalendar.getInstance(getTimeZoneFromDateTime(isoString, expanded));
        cal.setTime(isoToDate(isoString, expanded));
        cal.roll(field, up);
        return dateToISO(cal.getTime(), expanded);
    }

    public static final String roll(String isoString, int field, boolean up)
        throws ParseException
    {
        return roll(isoString, field, up, false);
    }

    public static final String timeToISO(Time date, boolean expanded)
    {
        SimpleDateFormat formatter;
        if(expanded)
            formatter = new SimpleDateFormat("HH:mm:ss,SSSzzz", dateFormatSymbles);
        else
            formatter = new SimpleDateFormat("HHmmssSSSzzz", dateFormatSymbles);
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        return formatter.format(date);
    }

    public static final String timeToISO(Time date)
    {
        return timeToISO(date, false);
    }

    public static final String timestampToISO(Timestamp date, boolean expanded)
    {
        SimpleDateFormat formatter;
        if(expanded)
            formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss,SSSzzz", dateFormatSymbles);
        else
            formatter = new SimpleDateFormat("yyyyMMdd'T'HHmmssSSSzzz", dateFormatSymbles);
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        return formatter.format(date);
    }

    public static final String timestampToISO(Timestamp date)
    {
        return timestampToISO(date, false);
    }

    /**
     * @deprecated Method toDate is deprecated
     */

    public static final Date toDate(float JD)
    {
        float Z = normalizedJulian(JD) + 0.5F;
        float W = (int)((Z - 1867216F) / 36524.25F);
        float X = (int)(W / 4F);
        float A = (Z + 1.0F + W) - X;
        float B = A + 1524F;
        float C = (int)(((double)B - 122.09999999999999D) / 365.25D);
        float D = (int)(365.25F * C);
        float E = (int)((double)(B - D) / 30.600100000000001D);
        float F = (int)(30.6001F * E);
        int day = (int)(B - D - F);
        int month = (int)(E - 1.0F);
        if(month > 12)
            month -= 12;
        int year = (int)(C - 4715F);
        if(month > 2)
            year--;
        Calendar c = Calendar.getInstance();
        c.set(1, year);
        c.set(2, month - 1);
        c.set(5, day);
        return c.getTime();
    }

    public static final float toJulian(Calendar c)
    {
        int Y = c.get(1);
        int M = c.get(2);
        int D = c.get(5);
        int A = Y / 100;
        int B = A / 4;
        int C = (2 - A) + B;
        float E = (int)(365.25F * (float)(Y + 4716));
        float F = (int)(30.6001F * (float)(M + 1));
        float JD = ((float)(C + D) + E + F) - 1524.5F;
        return JD;
    }

    public static final float toJulian(Date date)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return toJulian(c);
    }
    
    public static Date add(Date date, int field, int amount) {
    	Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(field, amount);
        return c.getTime();
    }

    protected static final float normalizedJulian(float JD)
    {
        float f = (float)Math.round(JD + 0.5F) - 0.5F;
        return f;
    }

    static {
        foo = new String[0][];
        dateFormatSymbles = new DateFormatSymbols();
        dateFormatSymbles.setZoneStrings(foo);
    }
}