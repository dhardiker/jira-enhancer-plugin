package com.tuncaysenturk.jira.plugins.report.valuegenerators;

import com.atlassian.configurable.ValuesGenerator;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.UserUtils;
import com.tuncaysenturk.jira.plugins.report.utils.PropertyReader;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author tsenturk
 * Class for users. Sorts the users by their names.<br>
 * <b>Notice:</b><br>
 */
public class UsersGroupsSelectValuesGenerator implements ValuesGenerator {

    private final GroupManager groupManager;

    public UsersGroupsSelectValuesGenerator(GroupManager groupManager) {
        this.groupManager = groupManager;
    }

    @SuppressWarnings("unchecked")
	public Map<NameSorterComparable, String> getValues(@SuppressWarnings("rawtypes") Map params) {
		Map<NameSorterComparable, String> map = new TreeMap<NameSorterComparable, String>();
		Collection<User> users = UserUtils.getAllUsers();
		User user;

		map.put(new NameSorterComparable(" ", "A"), "");
		for (Iterator<User> iterator = users.iterator(); iterator.hasNext();) {
			user = iterator.next();
			map.put(new NameSorterComparable(user.getName(), user.getDisplayName()), user.getDisplayName());
		}
		users.clear();
		Collection<Group> groups = groupManager.getAllGroups();
		for (Iterator<Group> iterator = groups.iterator(); iterator.hasNext();) {
			Group group = iterator.next();
			String prefix = PropertyReader.getInstance().getProperty("report.userhistory.groups.prefix");
			if (group != null) {
				map.put((new NameSorterComparable(prefix + group.getName(), group.getName())), group.getName());
			}
		}
		return map;
	}
}
