package com.tuncaysenturk.jira.plugins.report.utils;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * @author tsenturk
 * Convenient class for reading the .properties file. Use the .getInstance()
 * method to get a instance of the class.
 */

public class PropertyReader {
	private static final Logger logger = Logger.getLogger(PropertyReader.class);
	private static final String FILEPATH_PROERTIES_KEY = "com/tuncaysenturk/jira/plugins/report/userhistory/userhistory_report.properties";
	
	private static PropertyReader propertyReader;

	/**
	 * @return Returns the propertyReader.
	 */
	public static PropertyReader getInstance() {
		if (propertyReader == null) {
			propertyReader = new PropertyReader();
		}
		return propertyReader;
	}

	private Properties properties;

	private PropertyReader() {
	}

	private Properties getProperties() {
		if (properties != null)
			return properties;

		try {
			properties = new Properties();
			properties.load(getClass().getClassLoader().getResourceAsStream(
					FILEPATH_PROERTIES_KEY));
		} catch (IOException e) {
			logger.error(e);
		}
		return properties;
	}

	/**
	 * Returns a the String for a given key form the .properties file.
	 * 
	 * @param key
	 * @return
	 */
	public String getProperty(String key) {
		return this.getProperties().getProperty(key);
	}
}
