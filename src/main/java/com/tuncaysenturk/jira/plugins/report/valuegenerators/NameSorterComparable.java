package com.tuncaysenturk.jira.plugins.report.valuegenerators;

/**
 * @author tsenturk
 * Convenient class to use for examble with a TreeMap.<br>
 * Sorts by the viewName, but returns the name by overriding toString.<br>
 * Useful when you want to sort by something else than the key in the map, for
 * examble the corresponding value.
 */
public class NameSorterComparable implements Comparable<Object> {
	private String name, viewName;

	/**
	 * @param name
	 * @param viewName
	 */
	public NameSorterComparable(String name, String viewName) {
		this.name = name;
		this.viewName = viewName;
	}

	/**
	 * if (o instanceof NameSorterComparable == false) <div>return
	 * this.name.compareTo(o.toString());</div><br>
	 * return
	 * this.viewName.compareToIgnoreCase(((NameSorterComparable)o).getViewName());
	 * 
	 * @param o
	 * @return
	 */
	public int compareTo(Object o) {
		if (o instanceof NameSorterComparable == false)
			return this.name.compareTo(o.toString());

		return this.viewName.compareToIgnoreCase(((NameSorterComparable) o)
				.getViewName());
	}

	/**
	 * Returns the viewName
	 * 
	 * @return
	 */
	public String getViewName() {
		return this.viewName;
	}

	/**
	 * Returns name
	 */
	public String toString() {
		return this.name;
	}
}
