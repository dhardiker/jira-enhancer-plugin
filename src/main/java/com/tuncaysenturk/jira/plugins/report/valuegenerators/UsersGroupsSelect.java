package com.tuncaysenturk.jira.plugins.report.valuegenerators;

import com.atlassian.jira.user.util.UserManager;
import com.tuncaysenturk.jira.plugins.report.utils.PropertyReader;
import org.apache.log4j.Logger;

public class UsersGroupsSelect {
	private static final Logger logger = Logger.getLogger(UsersGroupsSelect.class);
	private static String groupPrefix = PropertyReader.getInstance().getProperty("report.userhistory.groups.prefix");
	
	public static boolean isGroup(String group) {
		return group.startsWith(groupPrefix);
	}
	
	public static String getGroupName(String group) throws Exception {
		if (isGroup(group))
			return group.substring(groupPrefix.length());
		else
			throw new Exception(group + " is not a valid group name");
	}
	
	public static boolean isValidUser(String userName, UserManager userManager) {
		try {
			logger.info("prefix: " + groupPrefix + "-userName:" + userName);

			if (null != userManager.getUser(userName))
				return true;
		} catch (Exception e) {
			logger.debug(userName + ": May be a group", e);
		}
		return isGroup(userName);
	}
}
