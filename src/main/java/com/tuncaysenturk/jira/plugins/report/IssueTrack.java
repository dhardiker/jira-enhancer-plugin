package com.tuncaysenturk.jira.plugins.report;

import java.sql.Timestamp;
import java.util.Iterator;
import java.util.Vector;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;

public class IssueTrack {
	private static final String ISSUE_CHANGE_DELIMITER = ", ";
	private Issue issue;
	private Timestamp timePerformed;
	private User user;
	private Vector<String> changes = new Vector<String>();
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Issue getIssue() {
		return issue;
	}
	public void setIssue(Issue issue) {
		this.issue = issue;
	}
	public Timestamp getTimePerformed() {
		return timePerformed;
	}
	public void setTimePerformed(Timestamp timePerformed) {
		this.timePerformed = timePerformed;
	}
	public void addChange(String change) {
		if (null != change)
			changes.add(changes.size(), change);
	}
	public String getChanges() {
		String ret = "";
		for (Iterator<String> iter = changes.iterator(); iter.hasNext();) {
			ret += iter.next(); 
			if (iter.hasNext()) ret += ISSUE_CHANGE_DELIMITER;
		}
		return ret;
	}
}
