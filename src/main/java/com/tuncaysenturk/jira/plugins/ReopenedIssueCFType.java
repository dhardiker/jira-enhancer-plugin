package com.tuncaysenturk.jira.plugins;
/**
 * @author tuncay.senturk
 */
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.security.JiraAuthenticationContext;
import org.ofbiz.core.entity.GenericEntity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
public class ReopenedIssueCFType extends AbstractJepCFType {
    public ReopenedIssueCFType(UserConverter userConverter,
			ChangeHistoryManager actionManager,
			JiraAuthenticationContext authenticationContext, DateTimeFormatterFactory dateTimeFormatterFactory) {
		super(userConverter, actionManager, authenticationContext, dateTimeFormatterFactory);
	}

	public String getStringFromSingularObject(Object o) {
        return (null == o) ? null : o.toString();
    }

    public Object getSingularObjectFromString(String s) throws FieldValidationException {
        return null;
    }

    public Object getValueFromIssue(CustomField customField, Issue issue) {
        ArrayList<ReopenedIssue> list = new ArrayList<ReopenedIssue>();
        User currentUser = authenticationContext.getLoggedInUser();
        try {
        	List<ChangeHistory> changes = changeHistoryManager.getChangeHistoriesForUser(issue, currentUser);
        	User reOpenedUser = null;
        	User resolvedUser = null;
        	Timestamp reOpenedTime = null;
        	Timestamp resolvedTime = null;
        	for (int i = 0; i < changes.size(); i++) {
        		ChangeHistory ch = changes.get(i);
        		List<GenericEntity> changeItems = ch.getChangeItems();
        		for (int j = 0; j < changeItems.size(); j++) {
        			GenericEntity changeItem = (GenericEntity) changeItems.get(j);
        			String ciField = (changeItem.containsKey("field") ? changeItem.getString("field") : "");
        			if (IssueFieldConstants.RESOLUTION.equals(ciField)) {
        				Object newValue = changeItem.get("newvalue");
        				Object oldValue = changeItem.get("oldvalue");
        				if (null == newValue && null != oldValue) {
        					reOpenedTime = ch.getTimePerformed();
        					reOpenedUser = ch.getAuthorUser();
        					ReopenedIssue item = new ReopenedIssue(authenticationContext.getOutlookDate(),
        							resolvedUser, reOpenedUser, resolvedTime, reOpenedTime);
        					list.add(item);
        				} else if (null != newValue && null == oldValue) {
        					resolvedTime = ch.getTimePerformed();
        					resolvedUser = ch.getAuthorUser();
        				}
        			}
        		}
        	}
        } catch (Exception e) {
        	logger.error("exception while getting change history", e);
        }
        if (list.size() == 0)
        	return null;
        return list;
    }
}
