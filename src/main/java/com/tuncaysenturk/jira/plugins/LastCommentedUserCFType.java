/**
 * @author tsenturk
 * MKK JIRA Plugin @version 1.0
 * Represents Last User that resolved the issue. 
 * Developer : Tuncay Senturk
 */

package com.tuncaysenturk.jira.plugins;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.security.JiraAuthenticationContext;

import java.util.List;

public class LastCommentedUserCFType extends AbstractJepUserCFType {

    private final CommentManager commentManager;

    public LastCommentedUserCFType(UserConverter userConverter,
                                   ChangeHistoryManager actionManager,
                                   JiraAuthenticationContext authenticationContext, DateTimeFormatterFactory dateTimeFormatterFactory, CommentManager commentManager) {
		super(userConverter, actionManager, authenticationContext, dateTimeFormatterFactory);
        this.commentManager = commentManager;
    }

	public Object getValueFromIssue(CustomField field, Issue issue) {  
    	User lastUser = null;
		try {
			User currentUser = authenticationContext.getLoggedInUser();  

			List<Comment> comments = commentManager.getCommentsForUser(issue, currentUser);
			if (comments != null && !comments.isEmpty())  {  
			    Comment lastComment = (Comment) comments.get(comments.size()-1);  
			    lastUser = lastComment.getAuthorUser();
			}
		} catch (Exception e) {
			logger.error("exception while getting last commented user", e);
		}  
       return lastUser;  
    }  
}
