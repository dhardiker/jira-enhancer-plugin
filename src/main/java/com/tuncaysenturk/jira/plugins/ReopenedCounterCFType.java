package com.tuncaysenturk.jira.plugins;

import java.util.ArrayList;

import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.security.JiraAuthenticationContext;

public class ReopenedCounterCFType extends ReopenedIssueCFType
{

	public String getStringFromSingularObject(Object o) {
        return (null == o) ? null : ((Double) o).toString();
    }

    public Object getSingularObjectFromString(String s) throws FieldValidationException {
        return (null == s) ? null : (new Double(s));
    }
	
    public ReopenedCounterCFType(UserConverter userConverter, ChangeHistoryManager changeHistoryManager, JiraAuthenticationContext authenticationContext, DateTimeFormatterFactory dateTimeFormatterFactory) {
		super(userConverter, changeHistoryManager, authenticationContext, dateTimeFormatterFactory);
	}

	@SuppressWarnings("rawtypes")
	public Object getValueFromIssue(CustomField customField, Issue issue) {
		ArrayList al = (ArrayList)super.getValueFromIssue(customField, issue);
		if (null == al)
			return new Double(0);
		else return new Double(al.size());
    }
}
