package com.tuncaysenturk.jira.plugins;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueAction;
import com.atlassian.jira.plugin.issuetabpanel.IssueTabPanelModuleDescriptor;

public class ReopeningsAction extends AbstractIssueAction {
	protected static final String TYPE_SGM = "sgm";
	protected static final String TYPE_YIM = "yim";
	protected static final String TYPE_DEFAULT = "default";
	
	protected final IssueTabPanelModuleDescriptor descriptor; 
	protected Timestamp timePerformed;
	protected List<ReopenedIssue> reopenings;
	protected String type;
	
	public List<ReopenedIssue> getReopenings() {
		return reopenings;
	}
	public void setReopenings(List<ReopenedIssue> reopenings) {
		this.reopenings = reopenings;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public ReopeningsAction(IssueTabPanelModuleDescriptor descriptor, String type, List<ReopenedIssue> reopenings) {
		super(descriptor);
		this.descriptor = descriptor;
		this.type = type;
		this.reopenings = reopenings;
	}
	public Date getTimePerformed() {
		return timePerformed;
	}
	@SuppressWarnings("unchecked")
	protected void populateVelocityParams(@SuppressWarnings("rawtypes") Map params) {
		params.put("action", this); 
	}
}
