package com.tuncaysenturk.jira.plugins;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueTabPanel;
import com.atlassian.jira.plugin.issuetabpanel.IssueAction;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.util.OutlookDateManager;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericEntity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReopeningsTabPanel extends AbstractIssueTabPanel {
	
	private static final Logger logger = Logger.getLogger(ReopeningsTabPanel.class);

    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final ChangeHistoryManager changeHistoryManager;
    private final OutlookDateManager outlookDateManager; //FIXME: Deprecated, needs to go

    public ReopeningsTabPanel(JiraAuthenticationContext jiraAuthenticationContext, ChangeHistoryManager changeHistoryManager, OutlookDateManager outlookDateManager) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.changeHistoryManager = changeHistoryManager;
        this.outlookDateManager = outlookDateManager;
    }

    public List<IssueAction> getActions(Issue issue, User remoteUser) {
		ArrayList<IssueAction> actions = new ArrayList<IssueAction>();
		ArrayList<ReopenedIssue> reopenings = new ArrayList<ReopenedIssue>();
        try {
        	List<ChangeHistory> changes = changeHistoryManager.getChangeHistoriesForUser(issue, remoteUser);

        	User reOpenedUser = null;
        	User resolvedUser = null;
        	Timestamp reOpenedTime = null;
        	Timestamp resolvedTime = null;
        	
        	for (int i = 0; i < changes.size(); i++) {
        		ChangeHistory ch = (ChangeHistory) changes.get(i);
        		@SuppressWarnings("unchecked")
				List<GenericEntity> changeItems = ch.getChangeItems();
        		for (int j = 0; j < changeItems.size(); j++) {
        			GenericEntity changeItem = (GenericEntity) changeItems.get(j);
        			String ciField = (changeItem.containsKey("field") ? changeItem.getString("field") : "");
        			
        			if (IssueFieldConstants.RESOLUTION.equals(ciField)) {
            			/**
            			 * (resolution = not null --> null)
            			 * resolvedUser is the user who changed resolution field by (null --> not null)
            			 * reopenedUser is the user who changed resolution field by (not null --> null)
            			 */ 
        				Object newValue = changeItem.get("newvalue");
        				Object oldValue = changeItem.get("oldvalue");
        				if (null == newValue && null != oldValue) {
        					// issue is reopened by any user
        					reOpenedTime = ch.getTimePerformed();
        					reOpenedUser = ch.getAuthorUser();
        					ReopenedIssue item = new ReopenedIssue(outlookDateManager.getOutlookDate(jiraAuthenticationContext.getLocale()),
        							resolvedUser, reOpenedUser, resolvedTime, reOpenedTime);
        					reopenings.add(item);
        				} else if (null != newValue && null == oldValue) {
        					// issue resolved by developer
        					resolvedTime = ch.getTimePerformed();
        					resolvedUser = ch.getAuthorUser();
        				}
        			}  
        		}
        	}
        } catch (Exception e) {
        	logger.error("exception while getting actions", e);
        }
        actions.add(new ReopeningsAction(descriptor, ReopeningsAction.TYPE_DEFAULT, reopenings));
        if (reopenings.size() == 0)
        	return Collections.emptyList();
        return actions;
	}

	public boolean showPanel(Issue issue, User remoteUser) {
		if (getActions(issue, remoteUser).isEmpty())
			return false;
		return true;
	}

}
