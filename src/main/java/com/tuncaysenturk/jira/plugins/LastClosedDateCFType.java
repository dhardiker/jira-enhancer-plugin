/**
 * @author tsenturk
 * MKK JIRA Plugin @version 1.0
 * Represents Last Closed Date of the issue. 
 * Developer : Tuncay Senturk
 */

package com.tuncaysenturk.jira.plugins;

import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.security.JiraAuthenticationContext;

@SuppressWarnings("unchecked")
public class LastClosedDateCFType extends AbstractJepDateCFType {
	public LastClosedDateCFType(UserConverter userConverter,
			ChangeHistoryManager actionManager,
			JiraAuthenticationContext authenticationContext, DateTimeFormatterFactory dateTimeFormatterFactory) {
		super(userConverter, actionManager, authenticationContext, dateTimeFormatterFactory);
		this.CUSTOM_FIELD_STATUS_ID = IssueFieldConstants.CLOSED_STATUS_ID;
	}
}
