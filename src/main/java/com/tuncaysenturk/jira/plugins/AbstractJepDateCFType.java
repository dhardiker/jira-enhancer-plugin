/**
 * @author tsenturk
 * Abstract date custom field 
 * Developer : Tuncay Senturk
 */

package com.tuncaysenturk.jira.plugins;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.DateField;
import com.atlassian.jira.security.JiraAuthenticationContext;

@SuppressWarnings("unchecked")
public abstract class AbstractJepDateCFType extends AbstractJepCFType implements DateField {
	
	public AbstractJepDateCFType(UserConverter userConverter,
			ChangeHistoryManager actionManager,
			JiraAuthenticationContext authenticationContext, DateTimeFormatterFactory dateTimeFormatterFactory) {
		super(userConverter, actionManager, authenticationContext, dateTimeFormatterFactory);
	}

    public String getStringFromSingularObject(Object value) {
//    	assertObjectImplementsType(Timestamp.class, value);
        return value.toString();
    }
    
    public Object getSingularObjectFromString(String string) throws FieldValidationException {
    	if (null == string)
    		return null;
    	Timestamp t = null;
		try {
			t = new Timestamp(
				new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS").parse(string).getTime());
		} catch (ParseException e) {
			logger.error("error converting to timestamp:" + string, e);
		}
        return t;
    }

    /** 
     * implement this method to get issue's last closed date.
     * @see com.atlassian.jira.issue.customfields.CustomFieldType#getValueFromIssue(com.atlassian.jira.issue.fields.CustomField, com.atlassian.jira.issue.Issue)
     */
    public Object getValueFromIssue(CustomField field, Issue issue) {
    	ChangeHistory ch = ((ChangeHistory)super.getValueFromIssue(field, issue));
    	if (ch != null)
    		return ch.getTimePerformed();
    	else 
    		return null;
    }
}
