/**
 * @author tsenturk
 * MKK JIRA Plugin @version 1.0
 * Represents Last Resolved Date of the issue. 
 * Developer : Tuncay Senturk
 */

package com.tuncaysenturk.jira.plugins;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.security.JiraAuthenticationContext;

import java.sql.Timestamp;
import java.util.List;

@SuppressWarnings("unchecked")
public class LastCommentedDateCFType extends AbstractJepDateCFType {

    private final CommentManager commentManager;

    public LastCommentedDateCFType(UserConverter userConverter,
                                   ChangeHistoryManager actionManager,
                                   JiraAuthenticationContext authenticationContext, DateTimeFormatterFactory dateTimeFormatterFactory, CommentManager commentManager) {
		super(userConverter, actionManager, authenticationContext, dateTimeFormatterFactory);
        this.commentManager = commentManager;
    }

    /** 
     * get issue's last commented date.
     * @see com.atlassian.jira.issue.customfields.CustomFieldType#getValueFromIssue(com.atlassian.jira.issue.fields.CustomField, com.atlassian.jira.issue.Issue)
     */
    public Object getValueFromIssue(CustomField field, Issue issue) {
        Timestamp lastCommentedDate = null;
		try {
			User currentUser = authenticationContext.getLoggedInUser();  
			List<Comment> comments = commentManager.getCommentsForUser(issue, currentUser);
			if (comments != null && !comments.isEmpty())  {  
			    Comment lastComment = comments.get(comments.size()-1);
			    lastCommentedDate = new Timestamp(lastComment.getCreated().getTime());
			}
		} catch (Exception e) {
			logger.error("exception while getting last commented date", e);
		}  
       return lastCommentedDate;  
    }
}
